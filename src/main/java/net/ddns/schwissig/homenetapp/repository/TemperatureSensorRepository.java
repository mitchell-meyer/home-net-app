package net.ddns.schwissig.homenetapp.repository;

import net.ddns.schwissig.homenetapp.entity.TemperatureSensor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Optional;

public interface TemperatureSensorRepository extends CrudRepository<TemperatureSensor, String> {

  @Transactional
  @Query(value = "SELECT * FROM TEMPERATURE_SENSOR WHERE NAME = ?1", nativeQuery = true)
  Optional<TemperatureSensor> findByName(String name);

  /**
   * Delete stale readings from the database.
   *
   * @param dateTime Removes all readings older than this time.
   */
  @Modifying
  @Transactional
  @Query(value = "DELETE FROM TEMPERATURE_SENSOR_TEMPERATURE_READINGS WHERE READING_TIME < ?1", nativeQuery = true)
  void deleteStaleData(LocalDateTime dateTime);
}