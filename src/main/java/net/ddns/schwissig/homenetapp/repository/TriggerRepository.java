package net.ddns.schwissig.homenetapp.repository;

import net.ddns.schwissig.homenetapp.entity.Trigger;
import org.springframework.data.repository.CrudRepository;

public interface TriggerRepository extends CrudRepository<Trigger, Long> {
}