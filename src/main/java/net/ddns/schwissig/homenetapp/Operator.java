package net.ddns.schwissig.homenetapp;

public enum Operator {

  LESS_THAN,
  EQUALS,
  GREATER_THAN
}
