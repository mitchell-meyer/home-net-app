package net.ddns.schwissig.homenetapp.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.LocalDateTime;

@Data
@Embeddable
public class Action {

  /** If the action is to send an email, this is the email address to send to. */
  @Column(name = "EMAIL_ADDRESS")
  private String emailAddress;

  /** Frequency that this action should occur, in seconds. */
  @Column(name = "FREQUENCY_IN_SECS")
  private long frequencyInSeconds;

  /** Time that this action was last executed. */
  @Column(name = "TIME_LAST_EXECUTED")
  private LocalDateTime timeLastExecuted;
}
