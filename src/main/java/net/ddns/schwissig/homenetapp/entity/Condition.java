package net.ddns.schwissig.homenetapp.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.math.BigDecimal;

@Data
@Embeddable
public class Condition {

  @Column(name = "SENSOR_ID")
  private String sensorId;

  @Column(name = "OPERATOR")
  private String operator;

  @Column(name = "VALUE")
  private BigDecimal value;

  /**
   * Check if this trigger is met.
   *
   * @return 'true' if this trigger is met, otherwise 'false'.
   */
  public boolean isMet(BigDecimal currentValue) {
    if ("<".equals(operator)) {
      return currentValue.compareTo(value) < 0;
    } else if ("=".equals(operator)) {
      return currentValue.compareTo(value) == 0;
    } else if (">".equals(operator)) {
      return currentValue.compareTo(value) > 0;
    }

    return false;
  }
}
