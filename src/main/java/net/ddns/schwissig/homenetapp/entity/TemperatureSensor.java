package net.ddns.schwissig.homenetapp.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

@Data
@Entity
@Table(name = "TEMPERATURE_SENSOR")
public class TemperatureSensor implements Serializable {

  @Id
  @Column(name = "IP")
  private String ipAddress;

  @Column(name = "NAME", unique = true)
  private String name;

  @Column
  @ElementCollection(targetClass = TemperatureReading.class, fetch = FetchType.EAGER)
  private List<TemperatureReading> temperatureReadings = new LinkedList<>();
}