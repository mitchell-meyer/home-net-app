package net.ddns.schwissig.homenetapp.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "TRIGGERS")
public class Trigger implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID")
  private long id;

  @Column(name = "DESCRIPTION")
  private String description;

  @Column
  @ElementCollection(targetClass = Condition.class, fetch = FetchType.EAGER)
  private Set<Condition> conditions = new HashSet<>();

  @Column
  @ElementCollection(targetClass = Action.class, fetch = FetchType.EAGER)
  private Set<Action> actions = new HashSet<>();
}
