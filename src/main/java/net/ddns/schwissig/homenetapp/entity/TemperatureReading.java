package net.ddns.schwissig.homenetapp.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Embeddable
public class TemperatureReading {

    @Column(name = "IP")
    private String ipAddress;

    @Column(name = "READING")
    private BigDecimal reading;

    @Column(name = "READING_TIME")
    private LocalDateTime readingTime;
}
