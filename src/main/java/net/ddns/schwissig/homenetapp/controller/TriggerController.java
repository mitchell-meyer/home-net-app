package net.ddns.schwissig.homenetapp.controller;

import net.ddns.schwissig.homenetapp.entity.Trigger;
import net.ddns.schwissig.homenetapp.service.TriggerService;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/trigger")
public class TriggerController {

  private static final Logger log = Logger.getLogger(TriggerController.class.getName());

  private final TriggerService triggerService;

  public TriggerController(TriggerService triggerService) {
    this.triggerService = triggerService;
  }

  @RequestMapping(value = "/save", method = RequestMethod.PUT)
  public void save(@RequestBody Trigger trigger) {
    triggerService.save(trigger);
  }
}