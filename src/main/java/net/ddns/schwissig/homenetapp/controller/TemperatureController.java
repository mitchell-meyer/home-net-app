package net.ddns.schwissig.homenetapp.controller;

import net.ddns.schwissig.homenetapp.entity.TemperatureReading;
import net.ddns.schwissig.homenetapp.entity.TemperatureSensor;
import net.ddns.schwissig.homenetapp.service.SensorService;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.*;

@RestController
@RequestMapping("/sensor/temperature")
public class TemperatureController {

  private static final Logger log = Logger.getLogger(TemperatureController.class.getName());

  private final SensorService sensorService;

  public TemperatureController(SensorService sensorService) {
    this.sensorService = sensorService;
  }

  @RequestMapping(value = "/saveSensorName", method = RequestMethod.PUT)
  public void saveSensorName(
      @RequestParam(value = "ipAddress") String ipAddress,
      @RequestParam(value = "name") String name) {
    log.info(String.format("Saving name of sensor with IP address [%s] to [%s].", ipAddress, name));

    // TODO Use returned boolean to return possible error code.
    sensorService.saveSensorName(ipAddress, name);
  }

  @RequestMapping(value = "/saveReading", method = RequestMethod.PUT)
  public void saveReading(@RequestBody TemperatureReading reading) {
    log.info(String.format("Reading received: ipAddress = [%s], reading = [%s],time = [%s]", reading.getIpAddress(), reading.getReading(), reading.getReadingTime().toString()));
    sensorService.saveReading(reading);
  }

  /**
   * Retrieve readings from a given temperature sensor.
   *
   * @param ipAddress IP address of the sensor for which to retrieve readings.
   * @param frequencyInMins Frequency between each readings in minutes.
   */
  @PutMapping(produces = {"application/json"})
  @RequestMapping("/getReading")
  public List<TemperatureReading> getReadings(
      @RequestParam(value = "ipAddress") String ipAddress,
      @RequestParam(value = "frequencyInMins") Integer frequencyInMins,
      @RequestParam(value = "maximumReadings") Integer maximumReadings) {
    Optional<TemperatureSensor> sensor = sensorService.getByIpAddress(ipAddress);
    if (sensor.isPresent()) {
      List<TemperatureReading> readingsToReturn = new ArrayList<>();

      List<TemperatureReading> currentReadings = sensor.get().getTemperatureReadings();
      ListIterator iterator = currentReadings.listIterator(currentReadings.size());
      LocalDateTime time = null;
      int readingsCount = 0;
      while (iterator.hasPrevious() && readingsCount < maximumReadings) {
        TemperatureReading reading = (TemperatureReading) iterator.previous();
        if (time == null) {
          time = reading.getReadingTime();
          readingsToReturn.add(reading);
          readingsCount++;
        } else if (reading.getReadingTime().isBefore(time.minusMinutes(frequencyInMins))) {
          time = time.minusMinutes(frequencyInMins);
          readingsToReturn.add(reading);
          readingsCount++;
        }
      }

      return readingsToReturn;
    } else {
      return Collections.emptyList();
    }
  }
}
