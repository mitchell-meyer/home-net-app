package net.ddns.schwissig.homenetapp.service;

import net.ddns.schwissig.homenetapp.EmailSender;
import net.ddns.schwissig.homenetapp.entity.*;
import net.ddns.schwissig.homenetapp.repository.TemperatureSensorRepository;
import net.ddns.schwissig.homenetapp.repository.TriggerRepository;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

@Service
public class TriggerService {

  private static final Logger log = Logger.getLogger(TriggerService.class.getName());

  private final TriggerRepository triggerRepository;

  private final TemperatureSensorRepository temperatureSensorRepository;

  public TriggerService(TriggerRepository triggerRepository, TemperatureSensorRepository temperatureSensorRepository) {
    this.triggerRepository = triggerRepository;
    this.temperatureSensorRepository = temperatureSensorRepository;
  }

  public void save(Trigger trigger) {
    triggerRepository.save(trigger);
  }

  /**
   * Checks to see if all conditions are met for any setup triggers. If so, the triggers actions are executed.
   */
  public void checkTriggers() {
    for (Trigger trigger : triggerRepository.findAll()) {
      if (isAllConditionsMet(trigger.getConditions())) {
        log.info(String.format("Trigger [%d]: All conditions met.", trigger.getId()));
        for (Action action : trigger.getActions()) {
          if (action.getTimeLastExecuted() == null || LocalDateTime.now().isAfter(action.getTimeLastExecuted().plusSeconds(action.getFrequencyInSeconds()))) {
            action.setTimeLastExecuted(LocalDateTime.now());
            log.info("Action happening.");
            String message = String.format("Trigger [%d]: All conditions met.", trigger.getId());
            new EmailSender(action.getEmailAddress(), message).run();
          }
        }
      } else {
        log.info(String.format("Trigger [%d]: Not all conditions met.", trigger.getId()));
      }

      triggerRepository.save(trigger);
    }
  }

  /**
   * Check a collection of conditions, returning 'true' if all conditions are met.
   *
   * @param conditions Collection of conditions to check.
   * @return 'true' if all conditions are met, otherwise 'false'.
   */
  private boolean isAllConditionsMet(Set<Condition> conditions) {
    for (Condition condition : conditions) {
      Optional<TemperatureSensor> readings = temperatureSensorRepository.findById(condition.getSensorId());
      if (readings.isPresent()) {
        int lastReadingIndex = readings.get().getTemperatureReadings().size();
        TemperatureReading latestReading = readings.get().getTemperatureReadings().get(lastReadingIndex - 1);
        if (latestReading != null && !condition.isMet(latestReading.getReading())) {
          return false;
        }
      } else {
        return false;
      }
    }
    return true;
  }


}
