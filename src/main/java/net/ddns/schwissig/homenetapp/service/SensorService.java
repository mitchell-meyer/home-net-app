package net.ddns.schwissig.homenetapp.service;

import net.ddns.schwissig.homenetapp.entity.TemperatureReading;
import net.ddns.schwissig.homenetapp.entity.TemperatureSensor;
import net.ddns.schwissig.homenetapp.repository.TemperatureSensorRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SensorService {

  private final TemperatureSensorRepository temperatureSensorRepository;

  public SensorService(TemperatureSensorRepository temperatureSensorRepository) {
    this.temperatureSensorRepository = temperatureSensorRepository;
  }

  /**
   * Save a temperature sensor name.
   *
   * @param ipAddress IP address of the temperature sensor to save.
   * @param name Name to give the temperature sensor.
   * @return 'true' if a name for a temperature sensor with the given IP address was saved successfully, otherwise 'false'.
   */
  public boolean saveSensorName(String ipAddress, String name) {
    Optional<TemperatureSensor> temperatureSensor = getByIpAddress(ipAddress);
    if (temperatureSensor.isPresent()) {
      temperatureSensor.get().setName(name);
      temperatureSensorRepository.save(temperatureSensor.get());
      return true;
    } else {
      return false;
    }
  }

  /**
   * Save a temperature reading for a temperature sensor.
   *
   * @param reading Temperature reading to save.
   */
  public void saveReading(TemperatureReading reading) {
    Optional<TemperatureSensor> temperatureSensor = getByIpAddress(reading.getIpAddress());
    if (temperatureSensor.isPresent()) {
      temperatureSensor.get().getTemperatureReadings().add(reading);
      temperatureSensorRepository.save(temperatureSensor.get());
    } else {
      TemperatureSensor sensor = new TemperatureSensor();
      sensor.setIpAddress(reading.getIpAddress());
      sensor.getTemperatureReadings().add(reading);
      temperatureSensorRepository.save(sensor);
    }
  }

  public Optional<TemperatureSensor> getByIpAddress(String ipAddress) {
    return temperatureSensorRepository.findById(ipAddress);
  }

  public Optional<TemperatureSensor> getByName(String name) {
    return temperatureSensorRepository.findByName(name);
  }
}
