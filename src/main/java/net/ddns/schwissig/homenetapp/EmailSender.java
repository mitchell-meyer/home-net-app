package net.ddns.schwissig.homenetapp;

import org.apache.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class EmailSender {

  private static final Logger log = Logger.getLogger(EmailSender.class.getName());

  private final String emailAddress;

  private final String emailMessage;

  public EmailSender(String emailAddress, String emailMessage) {
    this.emailAddress = emailAddress;
    this.emailMessage = emailMessage;
  }

  public void run() {
    String from = "home.net.do.not.reply@gmail.com";
    String password = "1Factory@";

    Properties props = new Properties();
    props.put("mail.smtp.host", "smtp.gmail.com");
    props.put("mail.smtp.socketFactory.port", "465");
    props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.port", "465");

    Session session = Session.getDefaultInstance(props,
        new javax.mail.Authenticator() {
          protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(from, password);
          }
        }
    );

    try {
      MimeMessage message = new MimeMessage(session);
      message.addRecipient(Message.RecipientType.TO,new InternetAddress(emailAddress));
      message.setSubject("HomeNet alarm");
      message.setText(emailMessage);

      Transport.send(message);
      log.info(String.format("Email successfully sent to [%s].", emailAddress));
    } catch (MessagingException e) {
      throw new RuntimeException(e);
    }
  }
}
