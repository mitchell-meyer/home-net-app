package net.ddns.schwissig.homenetapp;

import net.ddns.schwissig.homenetapp.repository.TemperatureSensorRepository;
import net.ddns.schwissig.homenetapp.service.TriggerService;
import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class ScheduledTasks {

  private static final Logger log = Logger.getLogger(ScheduledTasks.class.getName());

  private final TriggerService triggerService;

  private final TemperatureSensorRepository temperatureSensorRepository;

  public ScheduledTasks(TriggerService triggerService, TemperatureSensorRepository temperatureSensorRepository) {
    this.triggerService = triggerService;
    this.temperatureSensorRepository = temperatureSensorRepository;
  }

  @Scheduled(fixedRate = 5000)
  public void deleteStaleReadings() {
    temperatureSensorRepository.deleteStaleData(LocalDateTime.now().minusDays(30));
    log.info("Stale readings deleted.");
  }

  @Scheduled(fixedRate = 5000)
  public void checkTriggers() {
    triggerService.checkTriggers();
    log.info("Triggers checked.");
  }
}
