#include <DallasTemperature.h>

#include <OneWire.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <DallasTemperature.h>

#define ONE_WIRE_BUS 5 // DS18B20 pin

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature DS18B20(&oneWire);

const char* WIRELESS_SSID = "DasPad";
const char* WIRELESS_PASSWORD = "1drdeath2";

const int LOOP_COUNTER = 80000;

int LED_PIN = 14;
boolean powerLedOn;
int powerLedTimer = 0;

char temperatureString[6];

const char* broadcastIp = "192.168.1.255";
unsigned int udpBroadcastPort = 48618;

int broadcastTimer = LOOP_COUNTER;
WiFiUDP serverUdp;
char packetBuffer[UDP_TX_PACKET_MAX_SIZE];

/**
 * Sets up serial baud, WiFi, and temperature sensor. This will also auto-detect
 * and connect to the HomeNet server on the local network.
 */
void setup() {
  Serial.begin(115200);
  delay(10);
  Serial.println("Temperature sensor starting...");

  // Set up power LED.
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
  powerLedOn = false;

  connectToNetwork();

  // Start the UDP port listener.
  serverUdp.begin(udpBroadcastPort);

  // Temp sensor start.
  DS18B20.begin();
  Serial.println("Temperature sensor started.");
}

/**
 * Program loop that will run forever.
 */
void loop() {
  blinkPowerLed();
  
  if (WiFi.status() != WL_CONNECTED) {
    Serial.print("Connection to ");
    Serial.print(WIRELESS_SSID);
    Serial.print(" lost. Reconnecting...");
    connectToNetwork();
  }
  
  broadcastReading();
}

/**
 * Connects to the wireless network.
 */
void connectToNetwork() {
  // Connect to network.
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WIRELESS_SSID);
  
  WiFi.begin(WIRELESS_SSID, WIRELESS_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected.");
}

/**
 * Connects to the HomeNet main server via a specified broadcast port.
 */
void broadcastReading() {
  // If 500 cycles have elapsed since the last broadcast 'ping', then send another.
  if (broadcastTimer > LOOP_COUNTER) {
    byte packetBuffer[512];

    float temperature = getTemperature();
    dtostrf(temperature, 2, 2, temperatureString);

    // Send the UDP information.
    serverUdp.beginPacket(broadcastIp, udpBroadcastPort);
    serverUdp.print("tempReading:");
    serverUdp.print(temperatureString);
    serverUdp.endPacket();

    Serial.println("Broadcast ping sent.");

    // Reset the counter.
    broadcastTimer = 1;
  } else {
    broadcastTimer += 1;
  }
}

/**
 * 
 */
void blinkPowerLed() {
  if (powerLedTimer > LOOP_COUNTER) {
    if (powerLedOn == true) {
      digitalWrite(LED_PIN, LOW);
      powerLedOn = false;
    } else {
      digitalWrite(LED_PIN, HIGH);
      powerLedOn = true;
    }

    // Reset the timer.
    powerLedTimer = 1;
  } else {
    powerLedTimer = powerLedTimer + 1;
  }
}

/**
 * This will return a Fahrenheit tempature.
 */
float getTemperature() {
  float tempC, tempF;
  
  do {
    DS18B20.requestTemperatures(); 
    tempC = DS18B20.getTempCByIndex(0);
  } while (tempC == 85.0 || tempC == (-127.0));
  
  tempF = DallasTemperature::toFahrenheit(tempC);
  return tempF;
}
